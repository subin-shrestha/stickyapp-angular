import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { catchError } from 'rxjs/operators';
import { AlertService } from '../_services/alert.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthenticationService,
    private alertService: AlertService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
        console.log(err,'lll')
      if (err.status === 401) {
        this.authService.logout();
        location.reload(true);
      }

      else if (err.status === 0) {
        this.alertService.error('Unable to process your request currently!');
      }

      else if (err.status === 400) {
        if (err.error.non_field_errors) {
          const error_message = err.error.non_field_errors[0];
          this.alertService.error(error_message);
        }
        else {
          for (const key in err.error) {
            for (const message of err.error[key]) {
              const msg = key.replace('_', ' ').toLocaleUpperCase() + ': ' + message;
              this.alertService.error(msg);
            }
          }
        }
      }
      else {
        this.alertService.error(null);
      }
      return throwError(err);
    }));
  }
}

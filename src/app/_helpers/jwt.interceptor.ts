import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem('token');
        request = request.clone({
          url: environment.API_URL + request.url
        });
        if (token) {
            request = request.clone({
              setHeaders: {
                    Authorization: `Token ${token}`
                }
            });
        }
        return next.handle(request);
    }
}

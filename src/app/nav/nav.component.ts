import { Component, Input, Inject } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { MatDialog } from '@angular/material';

import { AuthenticationService } from '../_services'
import { DialogComponent } from '../dialog';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {
  @Input() isLoggedIn: boolean;

  user: Object;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    public dialog: MatDialog,
    private auth: AuthenticationService,
    private router: Router) {

    let user = localStorage.getItem('user');
    this.user = JSON.parse(user)
  }

  openLogoutDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {
        'title': 'Logout',
        'description': 'Are you sure to logout?',
        'cancelBtnCaption': 'Cancel',
        'confirmBtnCaption': 'Confirm'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.auth.logout();
        this.auth.isLoggedInObs();
        this.router.navigate(['']);
      }
    });
  }
}

export class Note {
    id: number;
    title: string;
    description: string;

    constructor(response) {
        this.id = response.id || null;
        this.title = response.title || '';
        this.description = response.description || '';
    }
}

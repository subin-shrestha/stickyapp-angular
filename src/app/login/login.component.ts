import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  nextUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthenticationService,
  ) { }

  ngOnInit() {
    // get return url from route parameters or default to '/'
    this.nextUrl = this.route.snapshot.queryParams['next'] || '/';
  }

  onSubmit(data: NgForm) {
    if (data.valid) {
      this.authService.login(data.value)
        .pipe(first())
        .subscribe(
          data => {
            this.authService.isLoggedInObs();
            this.router.navigateByUrl(this.nextUrl);
          });
    }
  }

}

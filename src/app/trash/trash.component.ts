import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { Note } from '../_models';
import { NoteService } from '../_services';
import { DialogComponent } from '../dialog';


@Component({
  selector: 'app-trash',
  templateUrl: './trash.component.html',
  styleUrls: ['./trash.component.css']
})
export class TrashComponent implements OnInit {
  queryString: string;
  notes: Array<Note>;

  constructor(
    private noteService: NoteService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loadNotes();
  }

  private loadNotes() {
    this.noteService.getDeletedNotes().subscribe(response => {
      this.notes = response;
    });
  }

  openSnackbar(msg) {
    if (this.queryString) {
      // Temporary hack for list refresh
      this.queryString = this.queryString + " ";
    }
    this.snackBar.open(msg, "", {
      duration: 3000,
    });
  }

  onDeleteDialog(id): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {
        'title': 'Delete',
        'description': 'Delete note forever?',
        'cancelBtnCaption': 'Cancel',
        'confirmBtnCaption': 'Confirm'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.onDelete(id)
      }
    });
  }

  onDelete(id) {
    let index = this.notes.findIndex(x => x.id === id);
    if (index > -1) {
      this.notes.splice(index, 1);
      this.noteService.destroy(id).subscribe(data => {
        //Open Snack Bar
        this.openSnackbar("Note " + id + " has deleted permanantly.");

      });
    }
  }

  onRestore(id) {
    let index = this.notes.findIndex(x => x.id === id);
    if (index > -1) {
      this.notes.splice(index, 1);
      this.noteService.restore(id).subscribe(data => {
        //Open Snack Bar
        this.openSnackbar("Note " + id + " has restored.");
      });
    }
  }

}

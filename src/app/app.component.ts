import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription} from 'rxjs';

import { AuthenticationService } from './_services'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'app';
  isLoggedIn: boolean;

  private subscription: Subscription;

  constructor(
    private authService: AuthenticationService,
  ) {}

  ngOnInit() {
    this.subscription = this.authService.isLoggedInObs()
      .subscribe(
        (value) => {
          this.isLoggedIn = value;
        });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

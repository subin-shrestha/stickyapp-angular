import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

 transform(items: any[], searchText: string): any[] {
   if (!items) return [];
   if (!searchText) return  items;
   if (searchText == '' || searchText == null) return [];
   
   searchText = searchText.trim();
    return items.filter((data) => this.matchValue(data, searchText))
 
  }

  matchValue(data, value) {
    return Object.keys(data).map((key) => {
        return new RegExp(value, 'gi').test(data[key]);
    }).some(result => result);
  }

}

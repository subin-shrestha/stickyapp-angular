import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Note } from '../_models';


@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor(private http: HttpClient) { }

  create(note) {
    return this.http.post<Note>(`api/note/`, note);
  }

  getAll() {
    return this.http.get<Note[]>(`api/note/`);
  }

  getDeletedNotes(){
    return this.http.get<Note[]>(`api/note/deleted`);
  }

  getById(id: number) {
    return this.http.get(`api/note/${id}`);
  }

  update(note: Note) {
    return this.http.put(`api/note/${note.id}/`, note);
  }

  delete(id: number) {
    // Soft delete, set is_deleted to true
    return this.http.delete(`api/note/${id}/delete/`);
  }

  restore(id: number) {
    return this.http.get(`api/note/${id}/restore/`);
  }

  destroy(id: number){
    return this.http.delete(`api/note/${id}`);
  }
}


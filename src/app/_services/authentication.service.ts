import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class AuthenticationService {
    private loggedIn = new BehaviorSubject<boolean>(false);

    constructor(private http: HttpClient) { }

    login(auth_credential) {
        return this.http.post<any>('api/auth/login/', auth_credential)
            .pipe(map(auth => {
                // login successful if there's a jwt token in the response
                if (auth && auth.key) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('token', auth.key);

                    this.http.get<any>('api/auth/user/')
                        .subscribe(user => {
                            localStorage.setItem('user', JSON.stringify(user));
                        });
                    
                }

                return auth;
            }));
    }

    isLoggedInObs() {
        if (localStorage.getItem('token')) {
            this.loggedIn.next(true);
        } else {
            this.loggedIn.next(false);
        }
        return this.loggedIn.asObservable();
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('token');
        localStorage.removeItem('user');
    }
}
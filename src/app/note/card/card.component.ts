import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';

import { Note } from '../../_models';
import { UpdateNoteComponent } from '../update-note';

import { NoteService } from '../../_services';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() note: Note;
  @Output() deleteItem = new EventEmitter();

  constructor(
    public dialog: MatDialog,
    private noteService: NoteService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  onDeleted(event) {
    event.stopPropagation();
    this.deleteItem.next(this.note);
  }

  onUpdate(note: Note): void {
    const dialogRef = this.dialog.open(UpdateNoteComponent, {
      width: '600px',
      data: {
        'title': note.title,
        'description': note.description,
        'cancelBtnCaption': 'Discard',
        'confirmBtnCaption': 'Update'
      }
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response) {
        let data = response
        data['id'] = this.note.id
        
        this.noteService.update(data).subscribe(note => {
          this.note = response;
          let message = `Note '${this.note.title}' has updated.`;
          let action = "";
          this.snackBar.open(message, action, {
            duration: 3000,
          });
        });
      }
    });
  }
}

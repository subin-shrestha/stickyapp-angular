import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


export interface ModalData {
  title: string;
  description: string;
  cancelBtnCaption: string;
  confirmBtnCaption: string;
}

@Component({
  selector: 'app-update-note',
  templateUrl: './update-note.component.html',
  styleUrls: ['./update-note.component.css']
})
export class UpdateNoteComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UpdateNoteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ModalData) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateNote(form: NgForm) {
    if (form.valid) {
      this.dialogRef.close(form.value);
    }
  }

}

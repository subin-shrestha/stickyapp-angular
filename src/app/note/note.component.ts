import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { Note } from '../_models';
import { NoteService } from '../_services';

import { AddNoteComponent } from './add-note';


@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {
  queryString: string;
  notes: Array<Note>;

  constructor(
    private noteService: NoteService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar) {
}

  ngOnInit() {
    this.loadNotes();
  }

  private loadNotes() {
    this.noteService.getAll().subscribe(response => {
      this.notes = response;
    });
  }

  addNote(): void {
    const dialogRef = this.dialog.open(AddNoteComponent, {
      width: '600px',
      data: {
        'title': '',
        'description': '',
        'cancelBtnCaption': 'Discard',
        'confirmBtnCaption': 'Save'
      }
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.noteService.create(data).subscribe(note => {
          this.notes.push(note);
        });
      }
    });
  }

  onDelete(note) {
    let note_id = note.id;
    let index = this.notes.findIndex(x=>x.id === note_id);
    if(index > -1){
      this.noteService.delete(note_id).subscribe(data => {
        this.notes.splice(index, 1);
        if(this.queryString){
          // Temporary hack for list refresh
          this.queryString = this.queryString + " ";
        }

        //Open Snack Bar
        let message = "Note " + note_id + " has moved to trash.";
        let action = "";
        this.snackBar.open(message, action, {
          duration: 3000,
        });
      });
    }
  }

}

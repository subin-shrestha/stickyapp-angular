import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule,
        MatListModule, MatCheckboxModule, MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';



import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AuthenticationService } from './_services';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';

import { AlertComponent } from './_directives';
import { LoginComponent } from './login';
import { NavComponent } from './nav';
import { HomeComponent } from './home';
import { NoteComponent } from './note';
import { CardComponent } from './note/card';
import { DialogComponent } from './dialog';
import { FilterPipe } from './_pipes';
import { AddNoteComponent } from './note/add-note';
import { UpdateNoteComponent } from './note/update-note';
import { TrashComponent } from './trash';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    LoginComponent,
    NavComponent,
    HomeComponent,
    NoteComponent,
    CardComponent,
    DialogComponent,
    FilterPipe,
    AddNoteComponent,
    UpdateNoteComponent,
    TrashComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LayoutModule,
    FormsModule,

    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCheckboxModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatSnackBarModule,
    MatTooltipModule,

    BrowserAnimationsModule,
  ],
  entryComponents: [ DialogComponent, AddNoteComponent, UpdateNoteComponent ],
  providers: [
    AuthenticationService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

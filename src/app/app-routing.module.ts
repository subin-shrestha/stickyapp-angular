import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { NoteComponent } from './note';
import { TrashComponent } from './trash';

import { AuthGuard, PreventAccess } from './_guards';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent, canActivate: [PreventAccess] },
  { path: 'notes', component: NoteComponent, canActivate: [AuthGuard]},
  { path: 'notes/trashed', component: TrashComponent, canActivate: [AuthGuard]},


  // Redirect all unmatched urls to home page
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


export const routing = RouterModule.forRoot(routes);